function copyURI(evt) {
    evt.preventDefault();
    navigator.clipboard.writeText(evt.target.getAttribute('href')).then(() => {
      /* clipboard successfully set */
    document.getElementById("urlForCopy").innerHTML = "copied";
    }, () => {
      /* clipboard write failed */
        document.getElementById("urlForCopy").innerHTML = "failed to copy :\(";
    });
}

