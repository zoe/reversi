pub struct Board {
    x : usize,
    y : usize,
    id : String,
    player_one: String,
    player_two: String,
    full : bool
}
