use rand::{thread_rng, Rng};
use rocket::serde::{Deserialize, Serialize};
use std::collections::HashMap;

#[derive(Debug)]
pub struct Room {
    pub token: usize,
    pub full: bool,
    pub play_queue: Vec<String>,
    pub board: Board,
}

impl Room {
    pub fn new() -> Room {
        // generate room token
        let token: usize = thread_rng().gen();
        Room {
            full: false,
            token,
            play_queue: vec![],
            board: Board::new(),
        }
    }
}

pub fn room_and_token_correct(
    rooms: &HashMap<String, Room>,
    roomname: &str,
    token: &usize,
) -> bool {
    if rooms.contains_key(roomname) {
        if &rooms.get(roomname).unwrap().token == token {
            return true;
        }
    }
    false
}

#[derive(Debug, Clone, FromForm, Serialize, Deserialize)]
#[cfg_attr(test, derive(PartialEq, UriDisplayQuery))]
#[serde(crate = "rocket::serde")]
pub struct Play {
    pub roomname: String,
    #[field(validate = len(2...2))]
    pub position: String,
    pub player1: bool,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
#[cfg_attr(test, derive(PartialEq))]
#[serde(crate = "rocket::serde")]
pub struct Board {
    pub position: String,
    pub player1: bool,
    pub fields: Vec<Vec<Field>>,
}

impl Board {
    pub fn new() -> Board {
        let mut rng = thread_rng();
        let mut fields: Vec<Vec<Field>> = vec![];
        let mut i: u8 = 0;
        while i <= 8 {
            let mut inner: Vec<Field> = vec![];
            let mut i_inner: u8 = 0;
            while i_inner <= 8 {
                inner.push(Field::new());
                i_inner += 1;
            }
            fields.push(inner);
            i += 1;
        }
        Board {
            player1: rng.gen_bool(1.0 / 2.0),
            // TODO change this!!! this doesn't make sense! i just put this here for testing
            position: "".to_string(),
            fields,
        }
    }
    pub fn make_play(&self, x: &u8, y: &u8) {
        //TODO
    }
    pub fn play_is_legal(&self, x: &u8, y: &u8, player1: &bool) -> bool {
        true
    }
}

#[derive(Debug, Clone, FromForm, Serialize, Deserialize)]
#[cfg_attr(test, derive(PartialEq, UriDisplayQuery))]
#[serde(crate = "rocket::serde")]
pub struct Field {
    owner: u8,
}


impl Field {
    fn new() -> Field {
        Field { owner: 0 }
    }
}
