use std::collections::HashMap;
use rocket_dyn_templates::Template;
use clap::Parser;

lazy_static! {
    static ref ARGS: Args = Args::parse();
}

#[derive(Parser, Debug)]
#[clap(about, version, author)]
struct Args {
    #[clap(short, long, default_value = "http://127.0.0.1:8000")]
    base_url: String,
}

pub fn get_back_to_home(roomname: &str) -> Template {
    join_template_from("sorry! looks like this room is already full!", "try another!", "./", "hidden", roomname)
}

pub fn join_new_room(roomname: &str, token: usize) -> Template {
    let message = format!("your room is \"{}\"! copy your link and send it to a friend to let them join", roomname);
    let link_url = link_url_from(roomname, token, true);
    join_template_from(&message, "let me in!", &link_url, "url_button", roomname)
}

pub fn join_room(roomname: &str, token: usize) -> Template {
    let message = format!("your room is \"{}\"! someone is already waiting in this room!", roomname);
    let link_url = link_url_from(roomname, token, false);
    join_template_from(&message, "let me in!", &link_url, "hidden", roomname)
}

pub fn get_room(roomname: &str, token: usize, player1: bool) -> Template{
    let mut context: HashMap<&str, &str> = HashMap::new();
    let tokstring = token.to_string();
    let p1string = player1.to_string();
    context.insert("roomname", roomname);
    context.insert("token", &tokstring);
    context.insert("player1", &p1string);
    Template::render("room", context)
}

fn join_template_from(message: &str, link: &str, link_url: &str, hide_url_button: &str, roomname: &str) -> Template {
    let url_button_url = &format!("{}/?roomname={}", ARGS.base_url, roomname);
    let mut context: HashMap<&str, &str> = HashMap::new();
    context.insert("message", message);
    context.insert("link", link);
    context.insert("link_url", link_url);
    context.insert("hide_url_button", hide_url_button);
    context.insert("url_button_url", url_button_url);
    Template::render("join", context)
}

fn link_url_from(roomname: &str, token: usize, player1: bool) -> String{
    format!("./room?roomname={}&token={}&player1={}", roomname, token, player1)
}
