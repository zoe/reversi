use std::io::{BufReader, BufRead};
use std::fs::File;
use rand::seq::SliceRandom;

lazy_static!{
    static ref NOUNS: Vec<String> = get_word_list("resources/nouns.txt");
    static ref ADJECTIVES: Vec<String> = get_word_list("resources/adjectives.txt");
}

pub async fn get_random_name() -> String{
    let rng = &mut rand::thread_rng();
    let noun: &String = NOUNS.choose(rng).expect("picking name failed");
    let adjective: &String = ADJECTIVES.choose(rng).expect("picking name failed");
    adjective.to_owned() + noun
}

fn get_word_list(path: &str) -> Vec<String> {
    let file = File::open(path).unwrap();
    let reader = BufReader::new(file);
    let mut words: Vec<String> = vec![];
    for line in reader.lines() {
        words.push(line.unwrap());
    }
    words
}
